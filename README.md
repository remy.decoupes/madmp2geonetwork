# Illustration de création d'une fiche de métadonnées à partir du modèle de données maDMP de DMP opidor

## Notes
Ce travail n'est qu'une illustration de ce que pourrait apporter le modèle de données maDMP. Le but est de construire une chaîne automatique de publication de données à partir du remplissage des DMP.

## Structuration du dépôt
* Script principal : **src/main.py** : Ce script va extraire les informations d'un fichier xml généré à partir du modèle xsd de maDMP
* Le précédent script fait appel à un autre script **src/AddServicesToGN.R** utilisant les librairies genoapi et geometa :
	* geonapi : R librairy to insert/update/delete metadata directly in our geonetwork : https://github.com/eblondel/geonapi
	* geometa : R librairy to create iso 19115 xml (inspire compliant) : https://github.com/eblondel/geometa
* Fichier xml compatible maDMP : **xml-manually-generated/maDMP_s.xsd.xml**

## Comment utiliser ce dépôt 
### Préparation environnement d'exécution
#### Python
Python 3
#### R
* Dependencies System libs:
```shell
sudo apt-get install libssl-dev libxml2-dev
```
* Dependencies System lib related to R software:
```shell
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
sudo add-apt-repository 'deb [arch=amd64,i386] https://cran.rstudio.com/bin/linux/ubuntu xenial/'
sudo apt-get install r-base
```
* R addiotional libs:
	(see https://github.com/eblondel/geonapi/wiki#install_guide, https://github.com/eblondel/geometa/wiki#install_guide)
```shell
install.packages("devtools")
install.packages("XML")
install.packages("uuid")
install.packages("osmdata")
install.packages("rjson")
require("devtools")
install_github("eblondel/geometa")
install_github("eblondel/geonapi")
```

### Paramétrisation 
1. Le fichier xml xml-manually-generated/maDMP_s.xsd.xml peut être configuré, en particulier les éléments researchOuput.
2. Les connexions au geonetwork ont été volontairement anonymisé dans le script **src/AddServicesToGN.R**, aussi veuillez à y insérer l'url - login -password

### Lancement
1. Extraction de données du fichier xml 
```shell
python3 src/main.py
```
2. Création fiche de métadonnées et upload geonetwork
```shell
/usr/bin/Rscript  addServicesToGN.R
```

## Remerciements
* Équipe DMP opidor
* Équipe Geoflow : librairies R de publication de fiche de métadonnées
* BlueBridge : Hébergement du geonetwork sur lequel sont publiées les fiches de métadonnéess
