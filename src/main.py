#!/usr/bin/env python

"""

"""
import xml.etree.ElementTree as ET
import csv
from uuid import uuid4

def main():
    """

    :return:
    """
    # Link to xml
    doc = ET.parse(("./../xml-manually-generated/maDMP_s.xsd.xml"))
    root = doc.getroot()

    # Link to csv : data pivot to R script
    csvfile = open("./../tmp/pivot.csv", "w")

    # Init row for csv
    infoextract = []
    n = 0
    # First row
    firstrow = []
    firstrow.append("n")
    firstrow.append("uuid")
    firstrow.append("title")
    firstrow.append("abstract")
    firstrow.append("keywords")
    infoextract.append(firstrow)

    for researchOutput in root.iter('researchOutput'):
        title, abstract = "", ""
        kw = []
        csvrow = []
        n = n + 1
        for subelement in researchOutput:
            #print(subelement.tag, subelement.text)
            if(subelement.tag == "title"):
                title = subelement.text
            elif(subelement.tag == "description"):
                abstract = subelement.text
            elif (subelement.tag == "uncontrolledKeyword"):
                kw.append(subelement.text)
        #csvrow.append((title, abstract, ""','.join(kw)+""))
        csvrow.append(n)
        csvrow.append(str(uuid4()))
        csvrow.append(title)
        csvrow.append(abstract)
        csvrow.append(""','.join(kw)+"")
        infoextract.append(csvrow)
    print(infoextract)

    with csvfile:
        writer = csv.writer(csvfile)
        for row in infoextract:
            writer.writerow(row)

if __name__ == "__main__":
    main()